libnetserver-generic-perl (1.03-10) UNRELEASED; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

 -- Ansgar Burchardt <ansgar@debian.org>  Wed, 27 Jul 2011 18:46:39 +0200

libnetserver-generic-perl (1.03-9) unstable; urgency=low

  * Take over for the Debian Perl Group on maintainer's request (via
    IRC).
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); ${misc:Depends} to Depends: field. Changed:
    Maintainer set to Debian Perl Group <pkg-perl-
    maintainers@lists.alioth.debian.org> (was: Gerfried Fuchs
    <rhonda@debian.at>).
    Replace versioned (build-)dependency on perl (>= 5.6.0-
    {12,16}) with an unversioned dependency on perl (as permitted by
    Debian Policy 3.8.3).
  * debian/watch: use dist-based URL and remove call to uupdate.
  * Switch to source format 3.0 (quilt).
  * Set Standards-Version to 3.9.2 (no further changes).
  * Use debhelper (8); use tiny debian/rules.
  * debian/copyright: DEP5 format.
  * Split out POD changes to proper patch. Revert changes in Makefile.PL.
  * Remove dependency on libtime-hires-perl, in perl core since quite some
    time.

 -- gregor herrmann <gregoa@debian.org>  Mon, 16 May 2011 23:04:57 +0200

libnetserver-generic-perl (1.03-8) unstable; urgency=low

  * debian/rules: Don't FTBFS when perl is smart enough not to create
    empty dirs. (Closes: #467990)
  * Add Homepage URL to source control part.
  * Updated to Standards-Version 3.7.3, changed Section to perl.
  * Add watch file.
  * Don't ignore make realclean errors.

 -- Gerfried Fuchs <rhonda@debian.at>  Mon, 07 Apr 2008 11:29:47 +0200

libnetserver-generic-perl (1.03-7) unstable; urgency=low

  * Args, forgot to change Maintainer field in debian/control, now really
    closes: #185525

 -- Gerfried Fuchs <alfie@debian.org>  Mon, 24 Mar 2003 10:22:05 +0100

libnetserver-generic-perl (1.03-6) unstable; urgency=low

  * New Maintainer (closes: #185525)
  * debian/control:
    - Updated to policy version 3.5.9
    - Removed fullstop from short description.
    - Build-Depends-Indep on perl, no debhelper.
  * debian/copyright:
    - removed superfluous (s) from Author.
  * debian/rules
    - undebhelperized it

 -- Gerfried Fuchs <alfie@debian.org>  Thu, 20 Mar 2003 17:27:28 +0100

libnetserver-generic-perl (1.03-5) unstable; urgency=low

  * Build in binary-indep (closes: #157495)
  * Add pointers to the Perl licenses (closes: #157625)

 -- Jon Middleton <jjm@debian.org>  Thu, 22 Aug 2002 20:00:30 +0100

libnetserver-generic-perl (1.03-4) unstable; urgency=low

  * New upload to fix broken diff from last version.
  * Cleaned up the rules file.

 -- Jon Middleton <jjm@debian.org>  Sat, 14 Jul 2001 15:25:29 +0100

libnetserver-generic-perl (1.03-3) unstable; urgency=low

  * Fixed Errors in Man page, example will now complie
  * Modified Build process to stop extra test being installed along 
    side NetServer::Generic
    
 -- Jon Middleton <jjm@debian.org>  Sat,  2 Jun 2001 15:46:23 +0100

libnetserver-generic-perl (1.03-2) unstable; urgency=low

  * Changed Maintainer address to debian.org

 -- Jon Middleton <jjm@debian.org>  Mon, 28 May 2001 17:59:34 +0100

libnetserver-generic-perl (1.03-1) unstable; urgency=low

  * New upstream release
  * Updated to lastest Policy

 -- Jon Middleton <jjm@ixtab.org.uk>  Tue, 27 Feb 2001 19:49:37 +0000

libnetserver-generic-perl (1.02-4) unstable; urgency=low

  * Updated to new Perl policy
  * Not released

 -- Jon Middleton <jjm@ixtab.org.uk>  Tue, 20 Feb 2001 22:20:49 +0000

libnetserver-generic-perl (1.02-3) unstable; urgency=low

  * Removed extra LICENSE file.
  * Renamed upstream Changes file to conform to policy.

 -- Jon Middleton <jjm@ixtab.org.uk>  Sun, 17 Dec 2000 15:48:39 +0000

libnetserver-generic-perl (1.02-2) unstable; urgency=low

  * Changed architecture to any from all.

 -- Jon Middleton <jjm@ixtab.org.uk>  Sun, 17 Dec 2000 15:48:09 +0000

libnetserver-generic-perl (1.02-1) unstable; urgency=low

  * New Upstream release.

 -- Jon Middleton <jjm@ixtab.org.uk>  Wed, 22 Nov 2000 16:21:09 +0000

libnetserver-generic-perl (1.00-1) unstable; urgency=low

  * Initial Release.

 -- Jon Middleton <jjm@ixtab.org.uk>  Tue, 16 May 2000 14:44:08 +0100


